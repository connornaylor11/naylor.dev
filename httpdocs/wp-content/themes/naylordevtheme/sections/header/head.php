<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="Description" content="Connor Naylor Development Website">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Connor Naylor Development</title>
    <meta name="application-name" content="Naylor Dev"/>
    <script src="https://kit.fontawesome.com/4ac2653cc2.js"></script>
    <?php wp_head(); ?>
</head>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "LocalBusiness",
        "address": {
            "@type": "PostalAddress",
            "addressLocality": "Accrington",
            "addressRegion": "United Kingdom",
            "postalCode":"BB5 5LE",
            "streetAddress": "28 Corn Mill Court"
        },
        "description": "Naylor Dev, the building blocks to your next site.",
        "name": "Naylor Dev",
        "priceRange": "£0-£50000",
        "url": "https://www.naylor.dev/",
        "logo": "https://naylor.dev/naylor/wp-content/themes/naylordevtheme/assets/img/logo.png",
        "image": "https://naylor.dev/naylor/wp-content/themes/naylordevtheme/assets/img/logo.png",
        "email": "mailto:info@naylor.dev",
        "telephone": "07739693349",
        "openingHours": "Mo-Fr 09:00-17:00",
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": "53.792284",
            "longitude": "-2.342459"
        },
        "sameAs" : [
            "https://www.facebook.com/connor.naylor.3",
            "https://www.instagram.com/devnaylor/",
            "https://twitter.com/devnaylor1",
        ]
    }
</script>
