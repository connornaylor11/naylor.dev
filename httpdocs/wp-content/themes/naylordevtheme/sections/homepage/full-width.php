<section class="full-width">
  <div class="container">
    <div class="row mb-5">
      <div class="col-12 white text-center">
        <h2>Code / Applications I use</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-4 white">
        <div class="inner-container">
          <div class="text-center mb-3"><i class="fas fa-code"></i></div>
          <h3>Development</h3>
          <ul>
            <li>HTML</li>
            <li>CSS</li>
            <li>Sass</li>
            <li>Bootstrap</li>
            <li>Jquery</li>
            <li>PHP</li>
            <li>Npm</li>
            <li></li>
            <li>Wordpress</li>
          </ul>
        </div>
      </div>
      <div class="col-4 white">
        <div class="inner-container">
          <div class="text-center mb-3"><i class="fas fa-palette"></i></div>
          <h3>Design</h3>
          <ul>
            <li>Sketch</li>
            <li>Photoshop</li>
            <li>Illustrator</li>
            <li>Invison</li>
          </ul>
        </div>
      </div>
      <div class="col-4 white">
        <div class="inner-container">
          <div class="text-center mb-3"><i class="fas fa-edit"></i></div>
          <h3>SEO</h3>
          <ul>
            <li>General SEO</li>
            <li>Google Analytics</li>
            <li>Heatmaps</li>
            <li>Yoast</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
