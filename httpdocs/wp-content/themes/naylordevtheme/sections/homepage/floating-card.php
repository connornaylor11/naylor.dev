<section class="floatingcard">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-7">
        <div class="space">
          <div class="card">
            <div class="card-shine"></div>
            <div class="code-snippet">
            <pre><code class="prism language-js">const terminal = StripeTerminal.create({
          onFetchConnectionToken: server.fetchConnectionToken,
        });

        const result = await terminal.discoverReaders();
        const reader = result.discoveredReaders[0];
        await terminal.connectReader(reader);

        const item = {
          description: 'High Growth Handbook',
          amount: 2000,
          quantity: 1,
        };
        const cart = {
          lineItems: [item],
          currency: 'usd',
        };

        terminal.setReaderDisplay({ type: 'cart', cart });</code></pre>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-md-5">
        <h2 class="mb-4">My Code</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
        <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
      </div>
    </div>
  </div>
</section>
