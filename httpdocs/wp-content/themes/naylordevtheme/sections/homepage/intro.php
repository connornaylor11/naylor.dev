<section class="intro">
  <div class="container">
    <div class="row">
      <div class="col-12 col-md-8">
        <h2 class="mb-4">Why work with me?</h2>
        <p>I'm a front end developer with a passion for creating bespoke websites. I love a challenge and I make it my goal to keep up to date witht the industry as much as I can.</p>
        <p>Although my main intrest is in development, I still contantly see what the latest design techniques are and how they can affect the UX of sites I create.</p>
        <div class="mt-6"><a class="button black" href="#">Portfolio</a><a class="button fill" href="#">Get In Touch</a></div>
      </div>
      <div class="col-12 col-md-4 mt-6 mt-md-0 text-center">
        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/code-graphic.jpg" alt="Code Graphic">
      </div>
    </div>
  </div>
</section>
