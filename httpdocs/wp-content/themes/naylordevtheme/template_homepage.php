<?php
/* Template Name: Homepage Template */
?>

<section class="header">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <img id="logo" src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/logo.png" alt="Logo">
        <nav class="hidden-sm-down">
          <a href="/about">ABOUT</a>
          <a href="/projects">PROJECTS</a>
          <a href="/blog">BLOG</a>
          <a href="/contact">CONTACT</a>
          <div id="indicator"></div>
        </nav>
      </div>
    </div>

  <section class="banner mb-lg-5">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-10">
          <h1 class="white mb-4">Front end <span>web developer</span> with a <span>passion</span> for <span>perfection</span></h1>
          <p>I enjoy creating user experiences that will provide the best results for you and your customers</p>
          <div class="mt-6"><a class="button" href="#">Get In Touch</a></div>
        </div>
      </div>
    </div>
  </section>
</section>

<?php get_header(); ?>


<?php get_template_part('sections/homepage/intro'); ?>
<?php get_template_part('sections/homepage/full-width'); ?>
<?php get_template_part('sections/homepage/floating-card'); ?>



<?php get_footer(); ?>
