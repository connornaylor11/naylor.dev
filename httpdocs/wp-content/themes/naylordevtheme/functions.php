<?php

//** Naylor Dev Functions
remove_action('wp_head', 'wp_generator');

// Remove Windows Live Writer link in header
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');

// Remove WP version info
function hide_wp_vers() {
	return '';
}
add_filter('the_generator','hide_wp_vers');

// Add Stylesheets
function queue_styles () {
	wp_register_style('style', get_stylesheet_directory_uri() . '/style.min.css', array(), time(get_stylesheet_directory() . '/style.min.css'));
	wp_enqueue_style('style');
}
add_action('wp_enqueue_scripts', 'queue_styles');

// Add Scripts
function queue_scripts () {

  wp_register_script('vendor', get_stylesheet_directory_uri() . '/assets/js/vendor.min.js', array('jquery'), true);
	wp_enqueue_script('vendor');
	wp_register_script('custom', get_stylesheet_directory_uri() . '/assets/js/custom.min.js', array('jquery', 'vendor',), true);
	wp_enqueue_script('custom');
}
add_action('wp_enqueue_scripts', 'queue_scripts');

if ( function_exists( 'add_image_size' ) ) {
	// add_image_size( 'news-thumbnail', 350, 228, true );
}

// Add Featured Image Support
add_theme_support( 'post-thumbnails' );

// Remove Width and Height from Featured / Uploaded imagesy

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}


// Add project post type

add_action( 'init', 'projects' );
function projects() {
	register_post_type( 'projects-posttype',
		array(
			'taxonomies' => array('topics', 'category' ),
			'labels' => array(
				'name' => __( 'Projects' ),
				'singular_name' => __( 'Project' ),
				'add_new' => __( 'Add New Project' ),
				'add_new_item' => __( 'Add New Project' ),
				'edit_item' => __( 'Edit Project' ),
				'new_item' => __( 'New Project' ),
				'all_items' => __( 'All Projects' ),
				'view_item' => __( 'View Project' ),
				'search_items' => __( 'Search Projects' ),
				'not_found' => __( 'Project not found' ),
				'not_found_in_trash' => __( 'Project not found in Trash' ),
				'parent_item_colon' => '',
				'menu_name' => __( 'Projects' )
			),
			'public' => true,
			'has_archive' => true,
			'hierarchical' => false,
			'show_in_nav_menus' => true,
			'menu_icon' => 'dashicons-hammer',
			'rewrite' => array('slug' => 'projects'),
			'supports' => array( 'title', 'editor', 'thumbnail', 'revisions' )
		)
	);
}

// Registering Menus

register_nav_menus( array(
      ) );

function wpb_custom_new_menu() {
  register_nav_menus(
    array(
      'menu' => __( 'Menu' ),
	    'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'wpb_custom_new_menu' );
