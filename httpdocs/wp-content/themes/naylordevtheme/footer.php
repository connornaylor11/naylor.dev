    <footer>
      <div class="container">
        <div class="row">
          <div class="col-12 text-center mb-5">
            <img id="logo" src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/logo.png" alt="Logo">
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-center mb-4">
            <a class="links" href="#">ABOUT</a>
            <a class="links" href="#">PROJECTS</a>
            <a class="links" href="#">BLOG</a>
            <a class="links" href="#">CONTACT</a>
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-center mb-2">
            <p>Number: 07739693349 | Email: connor@naylor.dev</p>
          </div>
        </div>
        <div class="row">
          <div class="col-12 text-center">
            <div class="footer-social-icons">
              <ul class="social-icons">
                <li><a href="" class="social-icon" aria-label="instagram"> <i class="fab fa-instagram"></i></a></li>
                <li><a href="" class="social-icon" aria-label="Twitter"> <i class="fab fa-twitter"></i></a></li>
                <li><a href="" class="social-icon" aria-label="Facebook"> <i class="fab fa-facebook-f"></i></a></li>
                <li><a href="" class="social-icon" aria-label="LinkedIn"> <i class="fab fa-linkedin"></i></a></li>
              </ul>
          </div>
        </div>
      </div>
    </footer>
  </body>
</html>
